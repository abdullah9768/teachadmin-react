import React, { useState } from 'react'
import classes from "../CSS/Sidebar.module.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { Link } from "react-router-dom";
import { useLocation } from 'react-router-dom/cjs/react-router-dom.min';

export const Sidebar = (props) => {
    const [sideBarState, setsideBarState] = useState({ onClick: true, state: "shown" })
    const [ArrowStyle, setArrowStyle] = useState({ transform: "rotate(180deg)" })
    const [sideBarWidth, setsideBarWidth] = useState({ width: "180px" })
    const [linkStyle, setlinkStyle] = useState({ opacity: "1" })
    const [sidebarStyle, setsidebarStyle] = useState({ top: "56px" })
    const [onOpen, setonOpen] = useState({marginRight:'180px'})


    let location = useLocation();
    const handleArrowInside = () => {
        if (!sideBarState.onClick) {
            setBarFunction("180px", false, "shown")
            setArrowStyle({ transform: "rotate(180deg)" })
            setlinkStyle({ opacity: "1" })
            setonOpen({marginRight:'180px'})
        }
    }
    const handleArrowOutside = () => {
        if (!sideBarState.onClick) {
            setBarFunction("30px", false, "hidden")
            setArrowStyle({ transform: "rotate(0deg)" })
            setlinkStyle({ opacity: "0" })
            setonOpen({marginRight: '30px'})
        }
    }
    const handleShowSideBar = () => {
        if (sideBarState.state === "hidden") {
            setBarFunction("180px", true, "shown")
            setArrowStyle({ transform: "rotate(180deg)" })
            setlinkStyle({ opacity: "1" })
            setonOpen({marginRight: '180px'})
        }
        else {
            setBarFunction("30px", false, "hidden")
            setArrowStyle({ transform: "rotate(0deg)" })
            setlinkStyle({ opacity: "0" })
            setonOpen({marginRight: '30px'})
        }
    }
    const setBarFunction = (width, onclick, state) => {
        setsideBarWidth({width: width})
        setsideBarState({ onClick: onclick, state: state })
    }
    window.onscroll =()=>{
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            setsidebarStyle({top: '0px'})
        }
        else{
            setsidebarStyle({top: '56px'})
        }
    }
    return (
        <>
            <div className={`${classes.mainSideBar} fixed inline-block`} style={sidebarStyle} >
                <div className={classes.sidebar} onMouseEnter={handleArrowInside} onMouseLeave={handleArrowOutside} style={sideBarWidth} >
                    <div className={`${classes.sideBarLinks} flex flex-col`} style={linkStyle}>
                        <Link className={`mb-4 min-w-max 4 pl-2 ${location.pathname === '/react/dashboard/' ? classes.active : ''}`} to="/react/dashboard/">Dashboard</Link>
                        <a className={`mb-4 min-w-max 4 pl-2 ${location.pathname === '/react/trainers-list/' ? classes.active : ''}`} href="https://teachadmin.ufaber.com/teachers/" target="_blank">Trainer’s List</a>
                        <a className={`mb-4 min-w-max 4 pl-2 ${location.pathname === '/react/students-list/' ? classes.active : ''}`} href="https://teachadmin.ufaber.com/students/" target="_blank">Student’s List</a>
                        <a className={`mb-4 min-w-max 4 pl-2 ${location.pathname === '/react/my-classes/' ? classes.active : ''}`} href="https://teachadmin.ufaber.com/all-classes-report/" target="_blank">My Classes</a>
                    </div>
                </div>
                <div className={classes.openSideBarBtn} onClick={handleShowSideBar} style={ArrowStyle} >
                    <FontAwesomeIcon icon={faArrowLeft} size="sm" />
                </div>
            </div>
            <div className="duration-700" style={onOpen}></div>    
        </>
    )
}
