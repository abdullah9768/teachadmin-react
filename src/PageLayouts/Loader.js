import React, { useEffect, useState } from 'react'
import Loader from "react-loader-spinner";
import { useSelector } from 'react-redux';
export const LoaderIcon = () => {
    const loaderStatus = useSelector(state => state.loaderStatus)
    const [loaderStyle, setloaderStyle] = useState({
        backgroundColor: '#000000ad',
        display:'none'
    })
    useEffect(() => {
       if(loaderStatus){
        setloaderStyle({...loaderStyle,zIndex:99, display:'block'})
       }
       else{
        setloaderStyle({...loaderStyle,zIndex:-99999, display:'none'})
       }
       //eslint-disable-next-line
    }, [loaderStatus])
    return (
        <div className="fixed top-0 h-full w-full left-0 bg-slate-50 -z-50" style={loaderStyle}>
            <div className="flex  justify-center absolute left-1/2 bottom-28 ">
            <Loader
                type="BallTriangle"
                color="white"
                height={100}
                width={100}
                visible={loaderStatus} //3 secs
            />
            </div>
            <h3 className="text-white text-center absolute bottom-14 left-1/2 ml-4">Loading</h3>
        </div>
    )
}
