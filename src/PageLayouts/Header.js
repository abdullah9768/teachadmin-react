import React from 'react'
import classes from '../CSS/Header.module.css'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { useEffect } from "react";
import { useLocation } from 'react-router-dom/cjs/react-router-dom.min';

export default function Header() {
    const FlagData = useSelector(state => state.FlagData)
    let location = useLocation()

    useEffect(() => {
     if(FlagData === 403 || location.pathname === '/'){
       window.location.href="https://teachadmin.ufaber.com/login/?next=/"
     }
     //eslint-disable-next-line
    }, [FlagData])

    return (
        <div className={`${classes.header} flex`}>
            <div className="left flex items-center p-2 pl-10">
                <img className='mr-6 ' src="https://s3-ap-southeast-1.amazonaws.com/ufaber-lms/custom/3fb1becae8744cb4a6d9fc395371a26f.png" alt="" />
               <Link to="/"><h2>uFaber</h2></Link> 
            </div>
            <div className="right flex items-center  ml-auto">
             <div className="name bg-sky-600 h-10 flex items-center  mr-4">
                 <a href="https://teachadmin.ufaber.com/referral/" target="_self" className="text-white inline-block px-8 w-full ">Referral</a>
             </div>
             <div className="name bg-rose-50 h-full flex items-center px-8">
                 <h3 className='text-black'>  {Object.keys(FlagData).length !== 0 ? FlagData.username:"Ansari"} </h3>
             </div>
            </div>
        </div>
    )
}
