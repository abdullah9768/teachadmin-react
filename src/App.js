import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Dashboard } from "./MainComponents/DashBoard/Dashboard";
import { TrainerList } from "./MainComponents/TrainerList/TrainerList";
import Header from "./PageLayouts/Header";
import { Sidebar } from "./PageLayouts/Sidebar";
function App() {

  return (
    <>
      <Router>

        <Header />
        <div className="flex">
          <Sidebar />
          <Switch>
            {/* <Route exact strict path="/">
            </Route> */}
            <Route exact path="/react/dashboard/">
              <Dashboard />
            </Route>
            <Route exact path="/react/trainers-list/" >
              <TrainerList />
            </Route>
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
