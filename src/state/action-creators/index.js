import Cookies from 'js-cookie'

const axios = require('axios');
let csrftoken = Cookies.get('csrftoken')
console.log(csrftoken)
export const flagsAction = (url) => {
    return (dispatch) => {
        let data
        axios.get(url, { headers: { "X-CSRFToken": csrftoken } })
            .then(function (response) {
                // handle success
                data = response.data
                console.log(data)
                dispatch({ type: 'fetchFlagsData', data: data })
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                data = error.response.status
                dispatch({ type: 'fetchFlagsData', data: data })

            })
            .then(function () {
            });
    }
}
export const dashboardFilterResult = (url) => {
    return (dispatch) => {
        let data
        axios.get(url, { headers: { "X-CSRFToken": csrftoken } })
            .then(function (response) {
                // handle success
                data = response.data
                console.log(data)
                dispatch({ type: 'fetchDataForDashBoard', data: data })
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                data = error.response.status
                dispatch({ type: 'fetchFlagsData', data: data })
            })
            .then(function () {
            });
    }
}
export const mappedCourseFetch = (url) => {
    return (dispatch) => {
        let data
        axios.get(url, { headers: { "X-CSRFToken": csrftoken } })
            .then(function (response) {
                // handle success
                data = response.data
                console.log(data)
                dispatch({ type: 'fetchForMappedCourse', data: data })
            })
            .catch(function (error) {
                // handle error
                data = error.response.status
                dispatch({ type: 'fetchFlagsData', data: data })
                console.log(error);
            })
            .then(function () {
            });
    }
}
export const actionSetURLParam = (URLParam) => {
    return (dispatch) => {
        dispatch({ type: 'setValForUrl', data: URLParam })
    }
}
export const loaderAction = (status) => {
    return (dispatch) => {
        dispatch({ type: 'loader', data: status })
    }
}