import { combineReducers } from "redux";
import DashboardFilterReducer from "./DashboardFilterReducer";
import Seturlreducers from "./seturlreducers";
import mappedCourseReducer from "./mappedCourseReducer";
import fetchFlagsReducer from "./fetchFlagsReducer";
import loaderReducer from "./LoaderReducer";


 const reducers = combineReducers({
    DashboardData:DashboardFilterReducer,
    URLParamData : Seturlreducers,
    MappedData : mappedCourseReducer,
    FlagData : fetchFlagsReducer,
    loaderStatus : loaderReducer,
})

export default reducers