import React from 'react'
import { DashboardBlock } from './DashboardBlock'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import classes from '../../CSS/Dashboard.module.css'
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actionCreators } from "../../state";
import { useSelector } from 'react-redux'
import { useEffect } from "react";
export const Sec1 = () => {
    const dispatch = useDispatch()
    const {flagsAction} = bindActionCreators(actionCreators, dispatch)
    const FlagData = useSelector(state => state.FlagData)
    console.log(FlagData,"ff")
    useEffect(() => {
        flagsAction(`https://teachadmin.ufaber.com/dc/red_flag/`)
       console.log(FlagData,"fd")
        // eslint-disable-next-line
    }, [])
    return (
        <div className="dashboardMain rounded-t-3xl   p-4 bg-rose-50 h-full w-full">
            <div className={classes.flags}>
                <h2 className="mr-4">Open Read flags</h2>
                <img className="mr-2" src="https://s3-ap-southeast-1.amazonaws.com/ufaber-lms/custom/1c32b18e22ee4ee1ad6820a57181b2ed.png" alt="" />
                <h2 className="mr-10">16</h2>

                <h2 className="mr-4">Total Red flags</h2>
                <img className="mr-2" src="https://s3-ap-southeast-1.amazonaws.com/ufaber-lms/custom/b45f54e9aa6342d5a0bb237e97e855e8.png" alt="" />
                <h2 className="mr-10">16</h2>

            </div>
            <div className="blocks flex flex-wrap">
            {Object.keys(FlagData).length !== 0 ?
            <>
            <DashboardBlock color={"bg-sky-500"} title= 'Negative Student FeedBack' value={FlagData.negative_student_feedback} link='/students/?redirect_via_dc=1&negative_feedback_students=1&status=1' />
            <DashboardBlock color={"bg-sky-600"} title = 'Low Daily Earnings'  value={FlagData.low_daily_earning} link="/teachers/?redirect_via_dc=1&low_daily_earnings=1" />
            <DashboardBlock color={"bg-sky-600"}  title = 'Low Demo Conversion Rate'  value={FlagData.low_demo_conversion_rate} link="/teachers/?redirect_via_dc=1&low_conversion_rate=1" />
            <DashboardBlock color={"bg-sky-600"}  title = 'Low Trainer Rating'  value={FlagData.low_rating} link="/teachers/?redirect_via_dc=1&low_ratings=1" />
            <DashboardBlock color={"bg-sky-600"}  title = 'New Trainers Without Students'  value={FlagData.no_student} link="/teachers/?redirect_via_dc=1&no_students=1" />
            <DashboardBlock color={"bg-sky-600"}  title = 'Off track trainers'  value={FlagData.offtrack_teacher} link="/teachers/?redirect_via_dc=1&off_track=1" />
            </>
            :"loading"
        }

            </div>
            <div className={classes.trainerWithRedFlags}>
                <span className="inline-block bg-sky-600 text-white text-xs w-7 h-7 p-1.5 mr-2 text-center rounded-full">12</span>
                <h2>Trainers With Red-Flags</h2>
                <span><FontAwesomeIcon icon={faChevronRight} size="sm" /></span>
            </div>
            <div className="blocks flex flex-wrap">
            {Object.keys(FlagData).length !== 0 ?
            <>
            <DashboardBlock color={"bg-green-300"}  title= 'Student Feedback  '   value={FlagData.student_feedback} />
            <DashboardBlock color={"bg-green-300"}  title = 'Off-track students'  value={FlagData.offtrack_student} link="/students/?redirect_via_dc=1&off_track_students=1&status=1"  />
            </>
            :"loading"
        }
            </div>
            <div className={classes.trainerWithRedFlags} style={{marginBottom:0}}>
                <span className="inline-block bg-green-400 text-white text-xs w-7 h-7 p-1.5 mr-2 text-center rounded-full">12</span>
                <h2>Students with Red-Flag</h2>
                <span><FontAwesomeIcon icon={faChevronRight} size="sm" /></span>
            </div>
        </div>
    )
}
