
import { MultiDatePicker } from "../MultiDatePicker";
import { TrainerType } from "../TrainerType";
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actionCreators } from "../../state";
import { useSelector } from 'react-redux'
import { useEffect } from "react";
import moment from 'moment';

export const Sec2 = () => {

    const dispatch = useDispatch()
    const { dashboardFilterResult, mappedCourseFetch, actionSetURLParam, loaderAction } = bindActionCreators(actionCreators, dispatch)
    const MappedData = useSelector(state => state.MappedData)
    const URLParamData = useSelector(state => state.URLParamData)
    useEffect(() => {
        mappedCourseFetch(`https://teachadmin.ufaber.com/dc/mapped/courses/`);
        let todays_date = moment(new Date()).format('DD-MM-YYYY')
        dashboardFilterResult(`https://teachadmin.ufaber.com/dc/overview/?start_date=${todays_date}&end_date=${URLParamData.to}&class_type=both&package=`)
        // eslint-disable-next-line
    }, [])
    const handleChangeCourse = (e) => {
        console.log(e.target.value)
        actionSetURLParam({ mappedCourseId: e.target.value })
    }
    const handleFilterApply = () => {
        loaderAction(true)
        console.log(URLParamData, "url")
        dashboardFilterResult(`https://teachadmin.ufaber.com/dc/overview/?start_date=${URLParamData.from}&end_date=${URLParamData.to}&class_type=${URLParamData.type}&package=${URLParamData.mappedCourseId}`)
    }
    return (
        <div className="flex  p-4 bg-white h-full w-full" style={{ boxShadow: ' 0px 2px 10px 0px #0000001A', marginBottom: '40px' }}>
            <div className="w-1/3">
                <TrainerType />
            </div>
            <div className="filters mr-2 w-1/5 pl-5">
                <div className="sec2-block">
                    <h2 className="mb-2">Mapped Courses</h2>
                    <div className="sec2-inner-block w-full ml-2 ">
                        <select name="" className="h-10 border-2 outline-0 w-4/5 rounded-lg" id='course-package' onChange={handleChangeCourse}>
                            <option hidden>--select course--</option>
                            {MappedData.length !== 0 ? MappedData.map((elem, index) => (
                                <option key={index}  value={elem.id}>{elem.name}</option>
                            )) : <option >No Data Available</option>}
                        </select>
                    </div>
                </div>
            </div>
            <div className="w-2/2" >
                <MultiDatePicker />
            </div>
            <div className="flex items-end w-1/12">
                <button className="w-full h-12 relative bottom-2 bg-sky-600 text-white ml-8 py-2 px-1 rounded-lg outline-0" onClick={handleFilterApply}>Apply</button>
            </div>
        </div>
    )
}
