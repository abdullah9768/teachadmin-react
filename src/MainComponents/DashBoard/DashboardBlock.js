import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faChevronRight } from '@fortawesome/free-solid-svg-icons'
export const DashboardBlock = (props) => {
    return (
        <div className="DashblockMain m-2 ">
            <a href={props.link}>
            <div className="w-60 overflow-hidden shadow-lg bg-white rounded-xl" style={{height:'108px'}}>
                <div className={`cardHeader ${props.color} w-full h-3`}>

                </div>
                <div className="px-6 py-2 h-16">
                    <p className="text-gray-700 text-base">
                       {props.title}
                    </p>
                </div>
                <div className="px-6 pb-2 flex items-center">
                   <span className="mr-auto "><img className="inline-block mr-2" src="https://s3-ap-southeast-1.amazonaws.com/ufaber-lms/custom/1c32b18e22ee4ee1ad6820a57181b2ed.png" alt="" /> <span className="font-semibold">{props.value}</span> </span>
                   <span><FontAwesomeIcon icon={faChevronRight} size="sm" /></span>
                </div>
            </div>
            </a>
        </div>
    )
}
