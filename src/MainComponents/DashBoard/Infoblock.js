import { faRupeeSign } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import classes from '../../CSS/Dashboard.module.css'
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actionCreators } from "../../state";
export const Infoblock = () => {
    const dispatch = useDispatch()
    const {loaderAction} = bindActionCreators(actionCreators, dispatch)
    const DashboardFilterData = useSelector(state => state.DashboardData)
    useEffect(() => {
        if(Object.keys(DashboardFilterData).length !== 0){
            loaderAction(false)
        }
        //eslint-disable-next-line
    }, [DashboardFilterData])

    console.log(DashboardFilterData, "dashboard")
    return (
        <>
        {Object.keys(DashboardFilterData).length !== 0 ?
            <>
                <div className={`${classes.infoblock} bg-rose-50 rounded-3xl   p-4  flex`}>
                    <div className="sec3-left w-2/5 mr-4">
                        <h2>Today’s Slot Info</h2>
                        <div className={`${classes.sec3text} flex mt-8`} >
                            <div className={classes.sec3block}>
                                <h3>Total Slots </h3>
                                <h4>{DashboardFilterData.slots_info.total_slots}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Total Available Slots</h3>
                                <h4>{DashboardFilterData.slots_info.total_available_slots}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Utilization of Inventory (%)</h3>
                                <h4>{DashboardFilterData.slots_info.percentage_utilization}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="sec3-right w-1/2">
                        <h2>Total Demo for the Month</h2>
                        <div className={`${classes.sec3text} flex mt-8`}>
                            <div className={classes.sec3block}>
                                <h3>Total Demo for the Month </h3>
                                <h4>{DashboardFilterData.slots_info.total_demo}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Total Demo Conversions</h3>
                                <h4>{DashboardFilterData.slots_info.total_demo_conversions}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Avarage Demo Convertion Rate</h3>
                                <h4>{DashboardFilterData.slots_info.average_demo_conversion_rate}%</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${classes.infoblock} bg-rose-50 rounded-3xl   p-4  flex`}>
                    <div className="sec3-left w-2/4 mr-4 pr-10">
                        <h2>Trainer Info</h2>
                        <div className={`${classes.sec3text} flex mt-8`} >
                            <div className={classes.sec3block}>
                                <h3>Active Trainers </h3>
                                <h4>{DashboardFilterData.trainer_info.active_trainers}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Trainers on Leave</h3>
                                <h4>{DashboardFilterData.trainer_info.leave_trainers}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Promotions Pending</h3>
                                <h4>{DashboardFilterData.trainer_info.leave_trainers}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Avarage Trainer Earnings</h3>
                                {/* <h4>  <FontAwesomeIcon icon={faRupeeSign} size="sm" className="mr-2"/>{DashboardFilterData.trainer_info.average_salaries.toFixed(2)}</h4> */}
                                <h4>N/A</h4>
                            </div>
                        </div>
                    </div>
                    <div className="sec3-right w-2/4">
                        <h2>Trainer Referal Info</h2>
                        <div className={`${classes.sec3text} flex mt-8`}>
                            <div className={classes.sec3block}>
                                <h3>Total Referral</h3>
                                <h4>{DashboardFilterData.trainer_info.total_referrals}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Referral converted</h3>
                                <h4>{DashboardFilterData.trainer_info.total_referrals_converted}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Referral Revenue</h3>
                                <h4>{DashboardFilterData.trainer_info.total_referrals_revenue}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Trainer sign-off pending</h3>
                                <h4>{DashboardFilterData.trainer_info.leave_trainers}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${classes.infoblock} bg-rose-50 rounded-3xl   p-4  flex`}>
                    <div className="sec3-left w-1/3 mr-4 pr-10">
                        <h2>Student Info</h2>
                        <div className={`${classes.sec3text}  flex mt-8`} >
                            <div className={classes.sec3block} style={{ width: '50%' }}>
                                <h3>No. of Students</h3>
                                <h4>{DashboardFilterData.student_attendation.student_count}</h4>
                            </div>
                            <div className={classes.sec3block} style={{ width: '50%' }}>
                                <h3>Students with Negetive Status</h3>
                                <h4>{DashboardFilterData.student_attendation.student_negative_feeedback}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="sec3-right w-3/5">
                        <h2>Student Who Need Attention</h2>
                        <div className={`${classes.sec3text} flex mt-8`}>
                            <div className={classes.sec3block} >
                                <h3>Trainer Feedback</h3>
                                <h4>{DashboardFilterData.student_attendation.trainer_feedback}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Low Attendance</h3>
                                <h4>{DashboardFilterData.student_attendation.low_attendance}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Low Content Consumption</h3>
                                <h4>{DashboardFilterData.student_attendation.low_consumption}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>Low Scores</h3>
                                <h4>{DashboardFilterData.student_attendation.low_score}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>No. of feedback by Students</h3>
                                <h4>{DashboardFilterData.student_attendation.number_of_feedbacks_by_students}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${classes.infoblock} bg-rose-50 rounded-3xl   p-4  flex`}>
                    <div className="sec3-left w-2/4 mr-4 pr-10">
                        <h2>Class Info</h2>
                        <div className={`${classes.sec3text} flex mt-8`} >
                            <div className={classes.sec3block}>
                                <h3>No. of Class Scheduled </h3>
                                <h4>{DashboardFilterData.classes_info.classes_scheduled}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>No. of Students Present</h3>
                                <h4>{DashboardFilterData.classes_info.students_present}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>No. of Students Absent</h3>
                                <h4>{DashboardFilterData.classes_info.students_absent}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>No. of Negetive feedback</h3>
                                <h4>{DashboardFilterData.classes_info.negative_feedback}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="sec3-right w-2/4">
                        <h2>Trainer Input Info</h2>
                        <div className={`${classes.sec3text} flex mt-8`}>
                            <div className={classes.sec3block}>
                                <h3>No. of Students Not Marked</h3>
                                <h4>{DashboardFilterData.classes_info.not_marked}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>No. of Recording not Available</h3>
                                <h4>{DashboardFilterData.classes_info.recordings_na}</h4>
                            </div>
                            <div className={classes.sec3block}>
                                <h3>No. of Classes with Short Duration</h3>
                                <h4>{DashboardFilterData.classes_info.short_classes}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </>
            : <h3 className='text-center mt-8 font-bold '>Please Select The Appropriate Filter</h3> }
        </>
    )
}
