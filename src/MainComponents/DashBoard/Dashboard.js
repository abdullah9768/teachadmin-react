import React from 'react'
import { LoaderIcon } from '../../PageLayouts/Loader'

import { Sec1 } from "./Sec1"
import { Sec2 } from "./Sec2"
import { Sec3 } from './Sec3'
export const Dashboard = () => {
    return (
        <div className="flex flex-col h-full  m-5  w-full">
            <Sec1 />
            <Sec2 />
            <div className="loader flex justify-center">
                <LoaderIcon />
            </div>
            <Sec3 />
        </div>
    )
}
