import React from 'react'
import { Infoblock } from './Infoblock'

export const Sec3 = () => {
    return (
        <div className="secInfo  h-full w-full">
            <Infoblock />
        </div>
    )
}
