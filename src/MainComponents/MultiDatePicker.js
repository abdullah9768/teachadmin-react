import React, { useState, useRef, useEffect } from "react";
import DatePicker, { DateObject } from "react-multi-date-picker";
import DatePanel from "react-multi-date-picker/plugins/date_panel";
import moment from 'moment';
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actionCreators } from "../state";

export const MultiDatePicker = () => {

    const dispatch = useDispatch()
    const { actionSetURLParam } = bindActionCreators(actionCreators, dispatch)
    const formattedDate = useRef({ from: "", to: "" })

    useEffect(() => {
        formattedDate.current = ({
            ...formattedDate.current,
            from: moment(new Date()).format('DD-MM-YYYY')
        })
        actionSetURLParam(formattedDate.current)
         // eslint-disable-next-line
    }, [])

    const format = "DD-MM-YYYY";
    const [dates, setDates] = useState([
        new DateObject().set(new Date()),
        // new Date().setDate(new Date().getDate() + 1),
    ]);
    const handleDateClick = (args) => {
        if (args === 'today') {
            setDates([
                new DateObject().set(new Date()),
            ]);
            formattedDate.current = ({
                ...formattedDate.current,
                from: moment(new Date()).format('DD-MM-YYYY'),
                to:""
            })
        }
        else {
            let date = new Date();
            console.log(moment(new DateObject().set(new Date())).format('DD-MM-YYYY'))
            setDates([
                new Date(date.getFullYear(), date.getMonth(), 1),
                new DateObject().set(new Date()),
                // new Date().setDate(new Date().getDate() - 30),
            ],
                formattedDate.current = ({
                    ...formattedDate.current,
                    from: moment(new Date()).format('DD-MM-YYYY'),
                    to: moment(new DateObject().set(new Date())).format('DD-MM-YYYY')
                })

            );

        }
        actionSetURLParam(formattedDate.current)
    }

    const handleDateChange = (param) => {
        param.map((date, key) => {
            console.log(date.format())
            if (key === 0) {
                formattedDate.current = ({
                    ...formattedDate.current,
                    from: date.format(),
                    to: ''
                })
            }
            if (key === 1) {
                formattedDate.current = ({
                    ...formattedDate.current,
                    to: date.format(),
                })
            }
            return formattedDate.current
        })
        actionSetURLParam(formattedDate.current)

    }

    return (
        <div className="filters mr-2 ">
            <div className="sec2-block">
                <h2 className="mb-2">Mapped Courses</h2>
                <div className="sec2-inner-block w-full ml-2 flex items-center ">
                    <button className="mr-5" onClick={() => handleDateClick("today")}>Today</button>
                    <button className="mr-5" onClick={() => handleDateClick("month")}>Month</button>
                    <DatePicker value={dates}
                        onChange={(arr) => { handleDateChange(arr) }}
                        multiple
                        sort
                        format={format}
                        calendarPosition="bottom-center"
                        range
                        plugins={[<DatePanel />]}
                        style={{ background: "#F1F1F1", height: '40px', outline: 'none', border: 'none', textAlign: 'center' }}
                    />


                </div>
            </div>
        </div>
    )
}
