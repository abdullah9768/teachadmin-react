import React, { useState } from 'react'

import Select from 'react-select';

export const MultiSelect = () => {
    const [selectedOption, setselectedOption] = useState()
    const options = [
        { value: 'Trainer Status', label: 'Trainer Status' },
        { value: 'No. of Active Students', label: 'No. of Active Students' },
        { value: 'Utilization of Weekly Slots', label: 'Utilization of Weekly Slots' },
        { value: 'No. of Demo Slots', label: 'No. of Demo Slots' },
        { value: 'Avg. Monthly Earning', label: 'Avg. Monthly Earning' },
        { value: 'Earning From:', label: 'Earning From:' },
        { value: 'Mapped Packages', label: 'Mapped Packages' },
    ];
    const handleChange = (selectedOption) => {
        setselectedOption(selectedOption );
    };
    
    return (
        <div className="flex items-center w-3/5">
            <label htmlFor="Smart Columns">Smart Columns</label>
            <Select
                value={selectedOption}
                onChange={handleChange}
                options={options}
                closeMenuOnSelect={false}
                placeholder="Select Columns"
                isMulti 
                className="w-3/4 ml-4"
            />
        </div>
    )
}
