import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actionCreators } from "../state";

export const TrainerType = () => {

    const dispatch = useDispatch()
    const { actionSetURLParam } = bindActionCreators(actionCreators, dispatch)

    const [btnStyle, setbtnStyle] = useState({both: {backgroundColor:'rgb(2 132 199 / var(--tw-bg-opacity))',color:'white'}})
    const handleClickTrainerType = (e) => {
        setbtnStyle({ [e.target.value]: {backgroundColor:'rgb(2 132 199 / var(--tw-bg-opacity))',color:'white'}})
        actionSetURLParam({type:e.target.value})
    }
    return (
        <div className="filters mr-2 ">
            <div className="sec2-block">
                <h2 className="mb-2">Trainer Type</h2>
                <div className="sec2-inner-block w-full flex ">
                    <button onClick={handleClickTrainerType} style={btnStyle.demo} value="demo" className="w-2/6  mr-4  py-2 px-1 rounded-lg outline-0" >Demo Trainers</button>
                    <button onClick={handleClickTrainerType} style={btnStyle.regular} value="regular" className="w-2/6  mr-4  py-2 px-1 rounded-lg outline-0" >Regular Trainers</button>
                    <button onClick={handleClickTrainerType} style={btnStyle.both} value="both" className="w-1/4  mr-0 py-2 px-1 rounded-lg outline-0" >Both</button>
                </div>
            </div>
        </div>
    )
}
