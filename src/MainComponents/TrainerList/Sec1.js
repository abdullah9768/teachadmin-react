import React from 'react'

export const Sec1 = () => {
    const handleDropDownChange = (e) => {
        e.target.style.color = 'black'
        console.log(e.target.value)
    }
    return (
        <div className="flex filters mr-2 w-full ">
            <div className="sec1-block w-1/5 mr-5">
                <h2 className="mb-4">Trainer Name</h2>
                <div className="sec1-inner-block w-full ml-2 ">
                   <input type="text" className="h-10 border-2 outline-0 w-full  rounded-lg pl-2" placeholder="Search Trainer Name" />
                </div>
            </div>
            <div className="sec1-block w-1/5 mr-5">
                <h2 className="mb-4">Mapped Courses</h2>
                <div className="sec1-inner-block w-full ml-2 ">
                    <select name="" className="h-10 border-2 outline-0 w-full rounded-lg" onChange={handleDropDownChange} >
                        <option hidden>--select course--</option>
                        <option >ZX</option>
                        <option >asd</option>
                    </select>
                </div>
            </div>
            <div className="sec1-block w-1/5 mr-5">
                <h2 className="mb-4">Mapped Courses</h2>
                <div className="sec1-inner-block w-full ml-2 ">
                    <select name="" className="h-10 border-2 outline-0 w-full rounded-lg" onChange={handleDropDownChange}>
                        <option hidden>--select course--</option>
                        <option >asd</option>
                        <option >asd</option>
                    </select>
                </div>
            </div>
            <div className="sec1-block w-1/5 mr-5">
                <h2 className="mb-4">Mapped Courses</h2>
                <div className="sec1-inner-block w-full ml-2 ">
                    <select name="" className="h-10 border-2 outline-0 w-full rounded-lg" onChange={handleDropDownChange}>
                        <option hidden>--select course--</option>
                        <option >asd</option>
                        <option >asd</option>
                    </select>
                </div>
            </div>
            <div className="sec1-block mr-5">
                <h2 className="mb-4">Mapped Courses</h2>
                <div className="sec1-inner-block w-full ml-2 ">
                    <select name="" className="h-10 border-2 outline-0 w-full rounded-lg" onChange={handleDropDownChange}>
                        <option hidden>--select course--</option>
                        <option >asd</option>
                        <option >asd</option>
                    </select>
                </div>
            </div>
            <div className="sec1-block mr-5">
                <h2 className="mb-4">Mapped Courses</h2>
                <div className="sec1-inner-block w-full ml-2 ">
                    <select name="" className="h-10 border-2 outline-0 w-full rounded-lg" onChange={handleDropDownChange}>
                        <option hidden>--select course--</option>
                        <option >asd</option>
                        <option >asd</option>
                    </select>
                </div>
            </div>
        </div>
    )
}
