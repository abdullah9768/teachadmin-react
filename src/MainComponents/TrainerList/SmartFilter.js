import { MultiSelect } from "../MultiSelect"

export const SmartFilter = () => {

    return (
        <>
        <div className="flex items-center mt-8">
            <div className="sec1-block mr-8 flex items-center w-1/3">
                <h2 className=" w-1/2">Mapped Courses</h2>
                <div className="sec1-inner-block w-full ">
                    <select name="" className="h-10 border-2 outline-0 w-full rounded-lg">
                        <option hidden>--select course--</option>
                        <option >asd</option>
                        <option >asd</option>
                    </select>
                </div>
            </div>
            <MultiSelect />
            </div>
        </>
    )
}
