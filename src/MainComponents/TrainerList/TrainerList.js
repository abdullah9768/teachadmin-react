import React from 'react'
import { Sec1 } from './Sec1'
import { TrainerType } from '../TrainerType'
import { MultiDatePicker } from '../MultiDatePicker'
import { SmartFilter } from './SmartFilter'
export const TrainerList = () => {
    return (
        <div className="flex flex-col h-full  m-5  w-full">
            <Sec1 />
            <div className="flex mt-8">
                <div className="w-1/3 mr-20">
                    <TrainerType />
                </div>
                <div className="w-2/5" >
                    <MultiDatePicker />
                </div>
            </div>
            <SmartFilter />
        </div>
    )
}
